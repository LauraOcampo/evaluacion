﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Evaluacion.Models;

namespace Evaluacion.Controllers
{
    public class VinculacionsController : Controller
    {
        private EvaluacionContext db = new EvaluacionContext();

        // GET: Vinculacions
        public ActionResult Index()
        {
            return View(db.Vinculacions.ToList());
        }

        // GET: Vinculacions/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vinculacion vinculacion = db.Vinculacions.Find(id);
            if (vinculacion == null)
            {
                return HttpNotFound();
            }
            return View(vinculacion);
        }

        // GET: Vinculacions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vinculacions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoVinculacionLaboral,Horas,Estado")] Vinculacion vinculacion)
        {
            if (ModelState.IsValid)
            {
                db.Vinculacions.Add(vinculacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vinculacion);
        }

        // GET: Vinculacions/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vinculacion vinculacion = db.Vinculacions.Find(id);
            if (vinculacion == null)
            {
                return HttpNotFound();
            }
            return View(vinculacion);
        }

        // POST: Vinculacions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoVinculacionLaboral,Horas,Estado")] Vinculacion vinculacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vinculacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vinculacion);
        }

        // GET: Vinculacions/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vinculacion vinculacion = db.Vinculacions.Find(id);
            if (vinculacion == null)
            {
                return HttpNotFound();
            }
            return View(vinculacion);
        }

        // POST: Vinculacions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Vinculacion vinculacion = db.Vinculacions.Find(id);
            db.Vinculacions.Remove(vinculacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
