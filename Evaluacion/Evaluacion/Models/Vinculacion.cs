﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion.Models
{
    public class Vinculacion
    {
        [Key]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Tipo de vinculación laboral")]
        public string TipoVinculacionLaboral { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public int Horas { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public Boolean Estado { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }
    }
}