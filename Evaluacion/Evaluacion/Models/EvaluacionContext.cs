﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Evaluacion.Models
{
    public class EvaluacionContext : DbContext
    {
        public EvaluacionContext() : base("DefaultConnection")
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public System.Data.Entity.DbSet<Evaluacion.Models.Especialidad> Especialidads { get; set; }

        public System.Data.Entity.DbSet<Evaluacion.Models.Vinculacion> Vinculacions { get; set; }
        public System.Data.Entity.DbSet<Evaluacion.Models.Instructor> Instructors { get; set; }





    }
}
    
    
