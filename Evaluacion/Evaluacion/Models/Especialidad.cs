﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion.Models
{
    public class Especialidad
    {
        [Key]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = " Nombre de la Especialidad ")]
        public string NombreEspecialidad { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public bool Estado { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }
    }
}